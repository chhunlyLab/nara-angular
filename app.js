require("dotenv").config();
const { json } = require("express");
const express = require("express");
const app = express();
const webDir = __dirname + '/web/dist/web';
app.use(express.json({ limit: "50mb" }));

app.use(express.static(webDir));

app.set('view engine', 'pug');

app.use("/", (req, res) => {
    const response = {
      message: "running",
      code: 200  
    };
  return res
      .status(200)
      .send(response);
});

module.exports = app;