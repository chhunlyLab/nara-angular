import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.sass']
})
export class LogoComponent implements OnInit {

  @Input() logoName: string = '';
  @Output() nameClick = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  onLogoClick(){
    this.nameClick.emit(this.logoName);
  }
}
