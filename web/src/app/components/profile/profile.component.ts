import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  myLogos: string[] = ['hello', 'hi', 'what"s your name?','why not me', 'crush', 'do you know me?'];
  childResponse: string = '';
  constructor() { }

  ngOnInit(): void {
  }
  nameClick(event: string){
    this.childResponse = event;
  }
}
